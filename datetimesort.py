import datetime
timestamps = {'2011-06-2':1,
		'2011-08-05':2,
		'2011-02-04':3,
		'2010-1-14':4,
		'2010-12-13':5,
		'2010-1-12':6,
		'2010-2-11':7,
		'2010-2-07':8,
		'2010-12-02':9,
		'2011-11-30':10,
		'2010-11-26':11,
		'2010-11-23':12,
		'2010-11-22':13,
		'2010-11-16':14
}

dates = [datetime.datetime.strptime(ts, "%Y-%m-%d") for ts in timestamps]
dates.sort()
sorteddates = [datetime.datetime.strftime(ts, "%Y-%m-%d") for ts in dates]
print("Original")
print(timestamps)
print("Sorted")
print(sorteddates)
